#ifndef PATHFINDER_SAMPLER_H
#define PATHFINDER_SAMPLER_H

#include <cstdint>

#define ITEMS_SIZE 10000

enum Direction {
    NORTH = 0,
    EAST = 1,
    SOUTH = 2,
    WEST = 3
};

struct Cell {
    unsigned row;
    unsigned col;
    unsigned direction;

    Cell() = default;

    Cell(unsigned row, unsigned col, unsigned direction) {
        this->row = row;
        this->col = col;
        this->direction = direction;
    }
};

struct Sampler {
    Sampler() = default;
    Sampler(int NUM_ROWS, int NUM_COLS);

    Cell items[ITEMS_SIZE];
    unsigned n;

    Cell sampleNoReplacement();
    bool checkWalls(uint8_t **maze, Cell cell);
    int encode(int r, int c, int d);
private:
    int numRows;
    int numCols;

    bool checkExteriorWall(Cell cell);
    bool checkExistenceWall(uint8_t **maze, Cell cell);
};
#endif // SAMPLER_H