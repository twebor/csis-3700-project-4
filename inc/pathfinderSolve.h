#ifndef PATHFINDER_PATHFINDERSOLVE_H
#define PATHFINDER_PATHFINDERSOLVE_H

#include <stack>
#include "sampler.h"
#include "pathfinder.h"
#include "disjointset.h"

extern unsigned NUM_ROWS;
extern unsigned NUM_COLS;

class PathfinderSolve {
public:
    PathfinderSolve(int argc, char **argv);
    void printMazeWrapper();
private:
    uint8_t **maze;
    Sampler sampler;
    std::stack<int> path;
    DisjointSet ds;

    void generateMaze();
    void findPath();
    Cell findAdjacentCell(Cell cell);
    void removeWall(Cell cell);
    bool wallExists(uint8_t row, uint8_t col, uint8_t dir);
};

#endif //PATHFINDER_PATHFINDERSOLVE_H
