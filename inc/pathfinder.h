#ifndef _PATHFINDER_H
#define _PATHFINDER_H

#include <cstdint>

const int
        MAX_ROWS = 50,
        MAX_COLS = 50;

enum CellValues {
    WALL_UP    = 0b0000'0001,
    WALL_RIGHT = 0b0000'0010,
    WALL_DOWN  = 0b0000'0100,
    WALL_LEFT  = 0b0000'1000,
    VISITED    = 0b0001'0000,
    DEAD_END   = 0b0010'0000
};

//=============================================================================
// void printMazeWrapper(uint8_t maze[][MAX_COLS],int nR,int nC);
//  print the given maze
//
// Parameters
//  maze - MAX_ROWS x MAX_COLS character array containing maze information
//  nR   - number of numRows in the maze
//  nC   - number of columns in the maze
//

void printMaze(uint8_t **maze,int nR,int nC);

//=============================================================================
// bool wallExists(uint8_t maze[][MAX_COLS],int r,int c,int d)
//  tell if a wall exists at the given edge of the given cell
//
// Parameters
//  maze - the maze
//  r    - row to check
//  c    - column to check
//  d    - direction to check
//
// Returns
//  true if a wall exists at the given location and direction, false otherwise
//

bool wallExists(uint8_t **maze,int r,int c,int d);

//=============================================================================
// bool cellVisited(uint8_t maze[][MAX_COLS],int r,int c)
//  tell if a cell has been visited during the solution process
//
// Parameters
//  maze - the maze
//  r    - row to check
//  c    - column to check
//
// Returns
//  true if the cell was visited during solution, false if not


bool cellVisited(uint8_t **maze,int r,int c);

#endif
