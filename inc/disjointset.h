#ifndef PATHFINDER_DISJOINTSET_H
#define PATHFINDER_DISJOINTSET_H

class DisjointSet {
public:
    DisjointSet() = default;
    DisjointSet(int size);

    int encode(int r, int c);
    std::pair<int, int> decode(int n);
    void setUnion(int a, int b);
    int find(int a);
private:
    int *elements;
    int *rank;
};

#endif // DISJOINTSET_H
