#include <iostream>
#include <ctime>
#include <random>
#include <cstdlib>
#include <pathfinder.h>
#include "pathfinderSolve.h"

unsigned NUM_ROWS;
unsigned NUM_COLS;

PathfinderSolve::PathfinderSolve(int argc, char **argv) {
    NUM_ROWS = std::stoi(argv[1]);
    NUM_COLS = std::stoi(argv[2]);
    sampler = Sampler(NUM_ROWS, NUM_COLS);

    if (NUM_ROWS > 50 || NUM_COLS > 50) {
        std::cerr << "Maze solver can only handle a maximum of 50 numRows and 50 columns." << std::endl;
        exit(1);
    }
    maze = new uint8_t*[NUM_ROWS]; // dynamic sized 2-d array. https://stackoverflow.com/questions/5508341/create-a-2d-array-with-variable-sized-dimensions
    for (auto i = 0; i < NUM_ROWS; i++) {
        maze[i] = new uint8_t[NUM_COLS];
    }
}

void PathfinderSolve::generateMaze() {
    int i = 0;
    for (int r = 0; r < NUM_ROWS; r++) {
        for (int c = 0; c < NUM_COLS; c++) {
            maze[r][c] = 0b1111; // 15
            for (int d = 0; d < 4; d++) {
                sampler.items[i++] = Cell(r, c, d);
            }
        }
    }
    ds = DisjointSet(NUM_ROWS * NUM_COLS);
    i = 0;
    sampler.n = NUM_ROWS * NUM_COLS * 4;
    Cell e, r2;
    while (i < NUM_ROWS * NUM_COLS - 1) {
        do {
            do {
                e = sampler.sampleNoReplacement();
            } while (!sampler.checkWalls(maze, e));
            // encode cell location: e = col + nCols * row
            r2 = findAdjacentCell(e);
        } while (ds.find(ds.encode(e.row, e.col)) == ds.find(ds.encode(r2.row, r2.col)));
        ds.setUnion(ds.encode(e.row, e.col), ds.encode(r2.row, r2.col));
        i++;

        removeWall(e);
    }
}

void PathfinderSolve::findPath() {
    path.push(sampler.encode(0, 0, 0));
    maze[0][0] |= VISITED;
    int r, c, d;
    int nextR, nextC = 0;
    while (true) {
        int n = path.top();
        r = (n / NUM_COLS) % NUM_ROWS;
        c = n  % NUM_COLS;
        d = n / (NUM_ROWS * NUM_COLS) ;
        if (r == NUM_ROWS - 1 && c == NUM_COLS - 1) {
            break;
        }

        if (d == 4) {
            maze[r][c] |= DEAD_END;
            path.pop();
        } else {
            switch (static_cast<Direction>(d)) {
                case NORTH:
                    nextR = r - 1;
                    nextC = c;
                    break;
                case SOUTH:
                    nextR = r + 1;
                    nextC = c;
                    break;
                case EAST:
                    nextC = c + 1;
                    nextR = r;
                    break;
                case WEST:
                    nextC = c - 1;
                    nextR = r;
                    break;
                default:
                    std::cerr << "Invalid direction - findPath" << std::endl;
                    exit(1);
            }
            path.pop();
            path.push(sampler.encode(r, c, d + 1));
            if (!wallExists(r, c, d) && !(maze[nextR][nextC] & VISITED)) {
                path.push(sampler.encode(nextR, nextC, 0));
                maze[nextR][nextC] |= VISITED;
            }
        }
    }
}

bool PathfinderSolve::wallExists(uint8_t row, uint8_t col, uint8_t dir) {
    switch (static_cast<Direction>(dir)) {
        case NORTH:
            return maze[row][col] & WALL_UP;
        case SOUTH:
            return maze[row][col] & WALL_DOWN;
        case EAST:
            return maze[row][col] & WALL_RIGHT;
        case WEST:
            return maze[row][col] & WALL_LEFT;
        default:
            return false;
    }
}

Cell PathfinderSolve::findAdjacentCell(Cell cell) {
    unsigned row = cell.row;
    unsigned col = cell.col;

    switch (cell.direction) {
        case NORTH:
            return Cell(row - 1, col, SOUTH);
        case SOUTH:
            return Cell(row + 1, col, NORTH);
        case EAST:
            return Cell(row, col + 1, WEST);
        case WEST:
            return Cell(row, col - 1, EAST);
        default:
            std::cerr << "Invalid direction - findAdjacentCell" << std::endl;
            exit(1);
    }
}

void PathfinderSolve::removeWall(Cell cell) {
    int row = cell.row;
    int col = cell.col;
    switch (static_cast<Direction>(cell.direction)) {
        case NORTH:
            maze[row][col] &= ~WALL_UP;
            maze[row - 1][col] &= ~WALL_DOWN;
            break;
        case SOUTH:
            maze[row][col] &= ~WALL_DOWN;
            maze[row + 1][col] &= ~WALL_UP;
            break;
        case EAST:
            maze[row][col] &= ~WALL_RIGHT;
            maze[row][col + 1] &= ~WALL_LEFT;
            break;
        case WEST:
            maze[row][col] &= ~WALL_LEFT;
            maze[row][col - 1] &= ~WALL_RIGHT;
            break;
        default:
            std::cerr << "Invalid direction - removeWall" << std::endl;
            exit(1);
    }
}

void PathfinderSolve::printMazeWrapper() {
    generateMaze();
    findPath();
    printMaze(maze, NUM_ROWS, NUM_COLS);
}
