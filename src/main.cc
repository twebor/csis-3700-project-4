#include <iostream>
#include "pathfinderSolve.h"

int main(int argc, char *argv[]) {
    PathfinderSolve solve = PathfinderSolve(argc, argv);
    solve.printMazeWrapper(); // printMaze() only has leftern and upper most walls.
    return 0;
}