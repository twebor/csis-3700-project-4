#include <cstdlib>
#include <random>
#include <iostream>
#include "sampler.h"
#include "pathfinder.h"

Sampler::Sampler(int NUM_ROWS, int NUM_COLS) {
    this->numRows = NUM_ROWS;
    this->numCols = NUM_COLS;
}

Cell Sampler::sampleNoReplacement() {
    std::random_device rd; // https://stackoverflow.com/questions/19665818/generate-random-numbers-using-c11-random-library
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist(0, RAND_MAX);

    unsigned i = dist(mt) % n;
    auto e = items[i];
    items[i] = items[--n];
    return e;
}

bool Sampler::checkWalls(uint8_t **maze, Cell cell) {
    return checkExteriorWall(cell) && checkExistenceWall(maze, cell);
}

bool Sampler::checkExteriorWall(Cell cell) {
    if (cell.col == 0 && cell.direction == WEST) {
        return false;
    } else if (cell.col == numCols - 1 && cell.direction == EAST) {
        return false;
    } else if (cell.row == 0 && cell.direction == NORTH) {
        return false;
    } else if (cell.row == numRows - 1 && cell.direction == SOUTH) {
        return false;
    }

    return true;
}

bool Sampler::checkExistenceWall(uint8_t **maze, Cell cell) {
    switch (cell.direction) {
        case NORTH:
            return maze[cell.row][cell.col] & WALL_UP;
        case SOUTH:
            return maze[cell.row][cell.col] & WALL_DOWN;
        case EAST:
            return maze[cell.row][cell.col] & WALL_RIGHT;
        case WEST:
            return maze[cell.row][cell.col] & WALL_LEFT;
        default:
            std::cerr << "Invalid direction" << std::endl;
            exit(1);
    }
}

int Sampler::encode(int r, int c, int d) {
    return (d * numRows + r) * numCols + c;
}

