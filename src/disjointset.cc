#include <pathfinderSolve.h>
#include "disjointset.h"

DisjointSet::DisjointSet(int size) {
    this->elements = new int[size];
    this->rank = new int[size];

    for (int i = 0; i < size; i++) {
        elements[i] = i;
        rank[i] = 0;
    }
}

void DisjointSet::setUnion(int a, int b) {
    a = find(a);
    b = find(b);

    if (a != b) {
        if (rank[a] < rank[b]) {
            elements[a] = b;
        } else if (rank[a] > rank[b]) {
            elements[b] = a;
        } else {
            rank[a] = rank[a] + 1;
            elements[b] = a;
        }
    }
}

int DisjointSet::find(int a) {
    if (elements[a] == a) {
        return a;
    } else {
        elements[a] = find(elements[a]);
        return elements[a];
    }
}

int DisjointSet::encode(int r, int c) {
    return r * NUM_COLS + c;
}

std::pair<int, int> DisjointSet::decode(int n) {
    return {n / NUM_COLS, n % NUM_COLS}; // (r, c)
}
